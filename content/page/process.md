---
title: Process
subtitle: ENGRco constantly researches, tests, uses, becomes, is its own open source product.
comments: false
---

[Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) is the practice of an organization using its own product ... and although we generally adhere to the philosophy behind "eating our own dogfood" the very first part of what ENGRco does is to look for better ways than the way we are doing something ... the first principle of our dogfooding process is to recognizing that somebody [in the open source world] is probably doing it better and we should look at that approach and possibly implement it here.  

ENGRco has a strong predisposition toward open source technologies; the source of all of our technology is FREE to anyone to extend, use, distribute. But we are not religious about this. We use proprietary technologies because we cannot afford to be blind to better approaches; we cannot afford not to know about other approaches.

ENGRco is fundamentally about Systems Engineering ... in order to understand the full Body of Knowledge, it might help for us to think about *traditional* [Systems Engineering](https://en.wikipedia.org/wiki/Systems_engineering) and the additional disciplines Applied [Complex Systems](https://en.wikipedia.org/wiki/Complex_system)

- [Self-organization](https://en.wikipedia.org/wiki/Self-organization)
- [Emergence](https://en.wikipedia.org/wiki/Emergence)
- Reductionism and Irreducibility
- Phase Transitions
- Agent-based Models, Networks, and Cellular Automata
- Graph Theory
- Nonlinear Dynamics
- Stability and Instability
- Ergodicity
- Chaos
- Probability
- Statistics
- Central Limit Theorem
- Independence and Interdependence
- Cascades
- Tails
- Stochastic Processes
- Random Walks
- Markov Chains
- Statistical Mechanics
- Cybernetics
- Information Theory
- Constraints
- Variety and Entropy
- Multiscale Variety
- Fractals
- Scaling properties and relations
- Pattern Formation
- Evolution
- Fragility and Antifragility
- Biological Development
- Computation and Formal Systems
- Syntax and Semantics
- Embodiment and Heuristics
- Theoretical Biology
- Autopoiesis and Relational Biology
- Anticipatory systems
- Perception and Affordances
- Human Organizations
- Military and Warfare
- Architecture and Building
- Software Development and Architecture
- Systemic Risk and Precaution
- Pandemic Response
- Policy and Society
- Decision-making under uncertainty
- Agriculture and Land Use
- Infrastructure
- Ecology and Climate
- Localism
- Trade
- Economics
- Innovation Processes
