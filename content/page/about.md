---
title: About ENGR.co
subtitle: ENGRco exists to LISTEN
comments: false
---

Our engineering objective is long-term antifragile sustainability ... the ideal is the dynamic elegance we find in Nature's change over time ... the incessant living continuity of Nature's *durable smoothness* is our objective ... we want our species to survive, but Nature will bat last.  

We look at actions and "skin in the game" allocations ... we particularly look at where or how people use their time ... although phatic communication is important, we generally try to eschew conversations about current events and politics -- there are only 86,400 s/day, we must focus on what matters most, prioritize our management of time accordingly ... the following is an approximate representation of the allocation of time that we strive for.


# Life-driven optimism and faith -- 45% or 75 hrs per week
15% ... Faith in Life, creation and the golden rule / sustainable, practical compassion rather than simplistic, idiot compassion / philosophy of kindness and non-aggression / science and its development with an eye where we can apply our efforts to sustain our faith, philanthropy, economics

15% ... Empowered health / active fitness and toughness / diet and fasting / extreme minimalism / distributed defense and security / martial arts and understanding of violence or force / overcoming atrophy, weakness and any sort of neediness or entitlement

15% ... Evolution of soil ecosystems / carbon-based terraforming / agricultural engineering / the future of farming / Nature and botanical intelligence 


# Data, engineering, the beauty of mathematics -- 30% or 50 hrs per week

12% ... Best practices for CALMS or Culture-Automation-LEAN-Measure-Sharing / the persistent diligence of continuous improvement / TRIZ or ARIZ and the macro-history or trends in development of patents, algorithms, standards

10% ... Data wrangling and the future of machine learning / open source communities like Git or Linux / observability engineering beyond mere tracking or DevSecOps / system-on-chip fabrication / engineering for IoT sensor networks that go beyond ARM / x86  or RISC-V to BRISC

8% ... Mathematics / evolution vs logic / antifragility and long-term elegance in Nature / the instruction set of Life / disciplined improvisation and frameworks for practice, eg jazz standards / legitimate artistic disruption with rejection of ***the need to intitiate*** revolution, war or violence

# Economics, markets, information -- 25% or 43 hrs per week

12% ... Open-source economics / venture philanthropy / post-capitalist finance / startups and nextgen jobs / commitgraphs and the wide variety of async workflows and lifestyle changes that are possible by eliminating the waste/stupidity of commuting and meetings 

8% ... Accelerated negotiations and learning / auctioneering and exchanges / crowdsourced "skin in the game" information replacements for mass media

5% ... Wikimedia/arXiv knowledge engineering / post-academic communities and reputation-driven fora / audio engineering and varied wavelengths of immersive learning 
