---
title: Chapter 11
subtitle: Antifragility
comments: false
---

How do you prospect or set things in motion to benefit from uncertainty?  This is different than the next chapter which is about fulfillment on orders, customer service, long-term subscriptions.