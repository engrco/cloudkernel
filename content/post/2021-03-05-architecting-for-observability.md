---
title: Architecting For Observability
subtitle: If the OS is focused, incoherence is visible
date: 2021-03-05
tags: ["example", "bigimg", "figma", "webassembly"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Let's think beyond just monitoring ... let's develop a foundation for engineering beyond the best practices in observability ... if we want our architecture to be more private, more secure, then we really have to think about architecture for observability RATHER than just putting security cameras on the same old architecture.