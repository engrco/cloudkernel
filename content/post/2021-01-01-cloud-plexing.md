---
title: Cloud Plexing
subtitle: There's more there than what you are able to see or think about
date: 2021-01-01
tags: ["signal", "noise"]
---

If you want to watch a bunch of binarythink wheels go completely off their two rails, give them something like [charlie-plexing](https://en.wikipedia.org/wiki/Charlieplexing) to think about ... and watch how the three states of this heretical system ABSOLUTELY HAVE TO BE pruned down to the two-states of binary logic ... the Universe uses a variety of multi-state logic, but humans who have been conditioned to see the world in terms of black and white or Off/On or binarythink are compelled to burn the proponents of more general, quantum forms of logic at the stake ... doing this will leave a mark, ie it really makes people uncomfortable to *force* them to examine their most fundamental, unexamined assumptions.

EVEN if humans can get past the realm of binarythink ... believe it or not, some folks do have some understanding of [how sampling clips the signal](http://www.wescottdesign.com/articles/Sampling/sampling.pdf) ... there are still relatively severe limitation [that we know about] of the human ability to percieve and think about things that they have not ever directly percieved with their own senses ... it's not just an RF thing or the well-known limits of our hearing, sight, smell, taste, touch or comprehension of our experience, it's also about the realms of things like dark matter, dark energy or how time works [or whether it's an illusion].