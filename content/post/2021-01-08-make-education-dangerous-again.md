---
title: Make Education Dangerous Again
subtitle: Why do we learn things
date: 2021-01-08
tags: ["example", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Education has become entirely about credentialling 

Think about what that means... competitive recursive hamster-wheel credentialling running like mad to make the cut to get into the next, more advanced round of credentialling ... and taking pride in the competition or scorekeeping about the hamster-wheel-spinning.

People do get this Reality in a small sense; that's why people plagiarize and teach their kids how to be master short-cutters and better plagiarizers ... that's why plagiarizers PROUDLY elect a Plagiarizer-In-Chief.

But education is basically about indoctrinating kids in order to do worthless one-brick-on-top-of-another-brick stuff that people adore, ie, "Look at the brick wall my kid built in brick wall science fair hackathon."

Education could be about something better ... it could be about making people legitimately dangerous.

Being legitimately dangerous is not about idiotic ghost-gunner nonsense which JUST terrifies people -- being legitimately dangerous is about learning how to learn and how to teach others to learn how to learn, so that you don't need an educatiion system ... thinking outside the box is about understanding the box well enough to construct it yourself, so that you and maybe a thousand friends can stand on that box and build the next level of box ... and being able to do this before a person is fifteen and then being able to do it for 100 years, to learn until a person is 115. 