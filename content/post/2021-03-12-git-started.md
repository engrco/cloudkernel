---
title: Git Started
subtitle: Figma like a DAG
date: 2021-03-12
tags: ["example", "bigimg", "figma", "webassembly"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Let's assume that you are already pretty familiar with the [basic fundamentals of Git](https://git-scm.com/book/en/v2) and, more generally, how we might use [direct acyclical graphs](https://en.wikipedia.org/wiki/Directed_acyclic_graph) ... if not, catch up.
